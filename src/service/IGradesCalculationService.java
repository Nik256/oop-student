package service;

import entity.Group;
import entity.Student;

public interface IGradesCalculationService {
    double calculateAverageGradeOfGroup(Group group);
    double calculateAverageGradeOfStudent(Student student);
    int getNumberOfExcellentStudents(Group group);
    int getNumberOfLoserStudents(Group group);
    boolean isStudentExcellent(Student student);
    boolean isStudentLoser(Student student);
}

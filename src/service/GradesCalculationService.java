package service;

import entity.Group;
import entity.Student;

public class GradesCalculationService implements IGradesCalculationService {
    @Override
    public double calculateAverageGradeOfGroup(Group group) {
        double sumOfAverageGrades = 0;
        double numberOfStudentsWithGrades = 0;
        double avg;
        for (Student student : group.getStudents()) {
            avg = calculateAverageGradeOfStudent(student);
            if (avg != 0) {
                sumOfAverageGrades += avg;
                numberOfStudentsWithGrades++;
            }
        }
        if (numberOfStudentsWithGrades != 0)
            return sumOfAverageGrades / numberOfStudentsWithGrades;
        return 0;
    }

    @Override
    public double calculateAverageGradeOfStudent(Student student) {
        int sumOfGrades = 0;
        int numberOfGrades = 0;
        for (int grade : student.getStudentProgress().getGrades())
            if (grade != 0) {
                sumOfGrades += grade;
                numberOfGrades++;
            }
        if (numberOfGrades != 0)
            return (double) sumOfGrades / numberOfGrades;
        return 0;
    }

    @Override
    public int getNumberOfExcellentStudents(Group group) {
        int numberOfExcellentStudents = 0;
        for (Student student : group.getStudents())
            if (isStudentExcellent(student))
                numberOfExcellentStudents++;
        return numberOfExcellentStudents;
    }

    @Override
    public int getNumberOfLoserStudents(Group group) {
        int numberOfLoserStudents = 0;
        for (Student student : group.getStudents())
            if (isStudentLoser(student))
                numberOfLoserStudents++;
        return numberOfLoserStudents;
    }

    @Override
    public boolean isStudentExcellent(Student student) {
        for (int grade : student.getStudentProgress().getGrades())
            if (grade < 5)
                return false;
        return true;
    }

    @Override
    public boolean isStudentLoser(Student student) {
        for (int grade : student.getStudentProgress().getGrades())
            if (grade < 3 && grade > 0)
                return true;
        return false;
    }
}

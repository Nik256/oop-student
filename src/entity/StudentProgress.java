package entity;

import java.util.Arrays;

public class StudentProgress {
    private int[] grades;

    public StudentProgress() {
        grades = new int[10];
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    @Override
    public String toString() {
        return "StudentProgress{" + "grades=" + Arrays.toString(grades) + '}';
    }
}

package entity;

import java.util.Arrays;

public class Group {
    private String name;
    private Student[] students;

    public Group(String name) {
        this.name = name;
        this.students = new Student[5];
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Student[] getStudents() {
        return students;
    }

    public void setStudents(Student[] students) {
        this.students = students;
    }

    public int getNumberOfStudents() {
        return students.length;
    }

    @Override
    public String toString() {
        return "Group{" + "name='" + name + '\'' + ", students=" + Arrays.toString(students) + '}';
    }
}

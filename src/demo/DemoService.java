package demo;

import entity.Group;
import entity.Student;
import entity.StudentProgress;
import service.IGradesCalculationService;

public class DemoService implements IDemoService {
    private IGradesCalculationService gradesCalculationService;

    public DemoService(IGradesCalculationService gradesCalculationService) {
        this.gradesCalculationService = gradesCalculationService;
    }

    public void execute() {
        Group group0 = new Group("SomeGroupName476346234");
        fillTheGroup(group0);
        System.out.println(group0);
        checkCalculationOfAverageGradeOfGroup(group0);
        checkCalculationOfAverageGradeOfStudent(group0.getStudents()[2]);
        checkGettingNumberOfExcellentStudents(group0);
        checkGettingNumberOfLoserStudents(group0);
        checkIsExcellentStudent(group0.getStudents()[1]);
        checkIsLoserStudent(group0.getStudents()[4]);
    }

    public void fillTheGroup(Group group) {
        Student[] students = new Student[5];
        group.setStudents(students);

        int[] grades = new int[]{3, 5, 5, 3, 4, 4, 5, 0, 2, 3};
        StudentProgress studentProgress = new StudentProgress();
        studentProgress.setGrades(grades);
        students[0] = new Student("Willy Wonka", studentProgress);

        grades = new int[]{5, 5, 5, 5, 5, 5, 5, 5, 5, 5};
        studentProgress = new StudentProgress();
        studentProgress.setGrades(grades);
        students[1] = new Student("Jane Doe", studentProgress);

        grades = new int[]{4, 5, 0, 4, 4, 4, 0, 0, 4, 5};
        studentProgress = new StudentProgress();
        studentProgress.setGrades(grades);
        students[2] = new Student("John Doe", studentProgress);

        grades = new int[]{3, 2, 0, 3, 3, 4, 2, 0, 1, 2};
        studentProgress = new StudentProgress();
        studentProgress.setGrades(grades);
        students[3] = new Student("Philip J. Fry", studentProgress);

        grades = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        studentProgress = new StudentProgress();
        studentProgress.setGrades(grades);
        students[4] = new Student("Nil Null", studentProgress);
    }

    @Override
    public void checkCalculationOfAverageGradeOfGroup(Group group) {
        System.out.println("-----------------------------------------------------------------------------------------");
        System.out.println("Average grade of group: " + gradesCalculationService.calculateAverageGradeOfGroup(group));
    }

    @Override
    public void checkCalculationOfAverageGradeOfStudent(Student student) {
        System.out.println("-----------------------------------------------------------------------------------------");
        System.out.println(student);
        System.out.println("Average grade of student: " + gradesCalculationService.calculateAverageGradeOfStudent(student));
    }

    @Override
    public void checkGettingNumberOfExcellentStudents(Group group) {
        System.out.println("-----------------------------------------------------------------------------------------");
        System.out.println("Number of excellent students: " + gradesCalculationService.getNumberOfExcellentStudents(group));
    }

    @Override
    public void checkGettingNumberOfLoserStudents(Group group) {
        System.out.println("-----------------------------------------------------------------------------------------");
        System.out.println("Number of loser students: " + gradesCalculationService.getNumberOfLoserStudents(group));
    }

    @Override
    public void checkIsExcellentStudent(Student student) {
        System.out.println("-----------------------------------------------------------------------------------------");
        System.out.println(student);
        System.out.println("This student is excellent? " + gradesCalculationService.isStudentExcellent(student));
    }

    @Override
    public void checkIsLoserStudent(Student student) {
        System.out.println("-----------------------------------------------------------------------------------------");
        System.out.println(student);
        System.out.println("This student is loser? " + gradesCalculationService.isStudentLoser(student));
    }
}

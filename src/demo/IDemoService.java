package demo;

import entity.Group;
import entity.Student;

public interface IDemoService {
    void checkCalculationOfAverageGradeOfGroup(Group group);
    void checkCalculationOfAverageGradeOfStudent(Student student);
    void checkGettingNumberOfExcellentStudents(Group group);
    void checkGettingNumberOfLoserStudents(Group group);
    void checkIsExcellentStudent(Student student);
    void checkIsLoserStudent(Student student);
}

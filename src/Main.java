import demo.DemoService;
import entity.Group;
import service.GradesCalculationService;

public class Main {

    public static void main(String[] args) {
        new DemoService(new GradesCalculationService()).execute();
    }
}
